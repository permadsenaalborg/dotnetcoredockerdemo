﻿module Example

open System

// Exercise #2
let rec count x =
    printfn "%A" x
    match x with
    | [] -> 0
    | h::t -> 1 + count t

let count_op x =
    let rec count_int n a =
        // printfn "%A" n
        match n with
        | [] -> 0
        | h::t -> count_int t a+1
    count_int x 0


// Exercise #3
let rec reverse x =
    // printfn "%A" x
    match x with
    | [] -> []
    | h::t -> (reverse t) @ [h]


// Exercise #4
let rec factorial x = 
    if x=0 then 
        0
    else if x=1 then 
        1
    else
        x * factorial (x-1) 

// Exercise #5
let rec fibonacci x = 
    // printf "%i\n" x
    match x with
    | 0 -> 0
    | 1 -> 1
    | n -> 
    fibonacci (n-1) + fibonacci (n-2)

// #4 tail recursive alternative
let factorial_tail n =
    let rec fact n acc =
        match n with
        | 0 -> acc
        | _ -> fact (n-1) (acc*n)
    fact n 1

// #5 tail recursive alternative
let rec fibonacci_tail x =
    let rec helper n1 n2 a =
        // printfn "%i %i %i" n1 n2 a
        match a with
        | 0 -> n1
        | n -> helper n2 (n2 + n1) (n - 1)
    helper 0 1 x


 // Exercise 7
let rec apply func lst =
    match lst with
    | [] -> []
    | h::t -> [func h] @ apply func t


let build str =
    "<td>" + str + "</td>"


let rec reduce lst =
    match lst with
    | [] -> ""
    | h::t -> h + reduce t

reduce (apply build ["Per"; "Ib"; "Poul"])


let mads (lst:_ list) (index:int) =
    lst.[index..]

let stringFromCharList (cl : char list) =
    String.concat "" <| List.map string cl

let incrChar (x : char) index = 
    char (index + int x)

let ceasar (str:string) idx=
    printfn "%s" str
    let rec ceasar_int str_list idx=
        match str_list with
        | [] -> []
        | h::t -> [incrChar h idx] @ ceasar_int t idx
    stringFromCharList (ceasar_int (Seq.toList str) idx)



[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"

    let mylist = [1;2;4;3;6;5;7;9;8;0]
    printfn "Count list with 10 elements"
    printfn "List: %A Elements %i" mylist (count mylist)

    printfn "Reverse list with 10 elements"
    printfn "List: %A Elements %A" mylist (reverse mylist)

    printfn "List with 10 factorial numbers"
    printfn "List: %A Elements" (apply factorial [1..10])
    printfn "List: %A Elements" (apply factorial_tail [1..10])

    printfn "List with 10 fibonacci numbers"
    printfn "List: %A Elements" (apply fibonacci [1..10])
    printfn "List: %A Elements" (apply fibonacci_tail [1..10])


    System.Console.ReadKey() |> ignore
    0 // return an integer exit code
