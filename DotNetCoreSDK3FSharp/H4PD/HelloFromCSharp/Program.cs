﻿using System;

namespace HelloFromCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World from C#!");
            Console.WriteLine("Factorial 10! {0}", Example.factorial(10));
            Console.WriteLine("Factorial_tail 10! {0}", Example.factorial_tail(10));
            Console.WriteLine("Fibonacci 10! {0}", Example.fibonacci(10));
            Console.WriteLine("Fibonacci_tail 10! {0}", Example.fibonacci_tail(10));
            Console.ReadKey();
        }
    }
}
