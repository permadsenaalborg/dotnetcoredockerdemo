
import java.lang.*;
import java.io.*;
import java.util.*;

import expression.parser.*;
import expression.lexer.*;
import expression.node.*;

public class Main {
  public static void main (String[] argv)
  {
    try {
      Lexer l = new Lexer (new PushbackReader (new BufferedReader(new FileReader(argv[0])), 1024));
      Parser p = new Parser (l);
      Start start = p.parse ();
      System.out.println (start.toString());

      Calculate calculator = new Calculate ();
      start.apply (calculator);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
};

